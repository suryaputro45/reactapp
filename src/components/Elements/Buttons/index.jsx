const Tombol = (props) =>{
    // konsep distractering dalam javascript (memecah properti langsung agar bisa di panggil tanpa prop.nameprop])
    // memberikan nilai default
    const {warna="violet",children="Tombol"}=props;
  
    return(
      // didalam backtip menggunakan dolar jika tidak tanpa dolar
    //   <button className={`h-10 px-6 font-semibold rounded-full bg-${props.warna}-600 text-white`} type="submit">
    //   {props.children}
    // </button>
    // jika sudah menggunakan nilai default
    <button className={`h-10 px-6 font-semibold rounded-full ${warna} text-white`} type="submit">
    {children}
  </button>
    )
  }
  export default Tombol;