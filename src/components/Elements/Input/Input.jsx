
const Inputan =(props) =>{
    return(
        <input type={props.type} className="text-sm border rounded w-full py-2 px-3 text-slate-700 placeholder:opcity-50" name={props.name} placeholder={props.placeholder} />
    );

}
export default Inputan;