import Label from "./Label"
import Inputan from "./Input"

const FormInputan =(props)=>{

    return(
        <div className="mb-6">
            <Label htmlFor={props.name}>{props.label}</Label>
            <Inputan type={props.type} name={props.name} placeholder={props.placeholder} /> 
        </div>
        
    )
}
export default FormInputan;