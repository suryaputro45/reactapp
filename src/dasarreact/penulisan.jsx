// class function
class Tombol extends React.Component{
    render(){
      return (
        <button className="h-10 px-6 font-semibold rounded-full bg-slate-600 text-white" type="submit">
        Buy now
      </button>
    
      )
    }
    }

    // function js 
    function TombolBaru() {
      return(
        <button className="h-10 px-6 font-semibold rounded-full bg-red-600 text-white" type="submit">
        Buy now
      </button>
     
      )
    }
    
    // array Functition 
    const TombolKeTiga = () =>{
      return(
        <button className="h-10 px-6 font-semibold rounded-full bg-violet-600 text-white" type="submit">
        Buy now
      </button>
      )
    }